CREATE TABLE IF NOT EXISTS `urls` (
 `id` bigint(20) AUTO_INCREMENT NOT NULL,
 `orig_url` varchar(500),
 `short_url` varchar(50),
 `created` date,
 `numhits` bigint(20),
 PRIMARY KEY (`id`)
);