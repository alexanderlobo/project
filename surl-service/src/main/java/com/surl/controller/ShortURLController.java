package com.surl.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.BasicLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.surl.model.ShortURLObj;
import com.surl.service.impl.ShortenURLServiceImpl;
import com.surl.util.ShortURLUtil;

@Controller
public class ShortURLController {

	@Autowired
	private ShortenURLServiceImpl shortenService;
	private static final BasicLogger logger = LoggerFactory.logger(ShortURLController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String displayForm(ShortURLObj request) {
		return "short";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView shortenUrl(HttpServletRequest httpRequest, @ModelAttribute ShortURLObj request,
			BindingResult bindingResult) {
		
		String url = request.getOriginalURL();

		if (!isValid(url)) {
			logger.error("Invalid URL : " + url);
		}

		ModelAndView modelAndView = new ModelAndView("short");
		
		long uniq = (long) Instant.now().toEpochMilli()/1000;
		System.out.println("Uniq  ---- " + uniq );
		request.setId( uniq );
		String encoded = ShortURLUtil.encodeURL( uniq );
	System.out.println("Uniq encoded - " + encoded);	
		request.setCreatedDate( LocalDateTime.now() );
		request.setNumHits(0L);
		request.setShortURL(encoded);
		
		System.out.println("Before saving... " + request);
		shortenService.saveURL(request);
		
		String prefix = "http://localhost:8080";
		
		modelAndView.addObject("shortenedUrl", prefix + "/redir/" + request.getShortURL());
		System.out.println("Getting back long URL : " + shortenService.findOriginalURLById(request.getId()));
		return modelAndView;
	}

	private boolean isValid(String urlString) {
		boolean isValid = true;
		try {
			new URL(urlString);
		} catch (MalformedURLException e) {
			isValid = false;
		}
		return isValid;
	}

	@RequestMapping(value = "/redir/{id}", method = RequestMethod.GET)
	public void redirectToUrl(@PathVariable String id, HttpServletResponse resp) throws Exception {
		long uniqID = ShortURLUtil.decodeURL(id);
		final String longUrl = shortenService.findOriginalURLById(uniqID);
		if (longUrl != null) {
			resp.addHeader("Location", longUrl);
			resp.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		} else {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}

}
