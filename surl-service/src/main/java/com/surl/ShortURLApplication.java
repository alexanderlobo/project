package com.surl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackageClasses=com.surl.model.ShortURLObj.class)
public class ShortURLApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShortURLApplication.class, args);
    }
}
