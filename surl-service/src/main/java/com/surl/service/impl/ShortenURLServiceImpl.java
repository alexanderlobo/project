package com.surl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.surl.dao.ShortenRequestDao;
import com.surl.model.ShortURLObj;

@Service
@Transactional
public class ShortenURLServiceImpl implements IShortenURLService {

	@Autowired
	private ShortenRequestDao shortenReqDao;

	public void saveURL(ShortURLObj urlObj) {
		shortenReqDao.save(urlObj);
	}
	
	public String findShortURLById(long id ) {
		ShortURLObj url = shortenReqDao.getURLById(id);
		return url.getShortURL();
	}
	
	public String findOriginalURLById(long id) {
		ShortURLObj url = shortenReqDao.getURLById(id);
		return url.getOriginalURL();
	}
}
