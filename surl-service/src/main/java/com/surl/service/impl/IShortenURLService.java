package com.surl.service.impl;

import com.surl.model.ShortURLObj;

public interface IShortenURLService {

	public void saveURL(ShortURLObj urlObj);
	public String findShortURLById(long id );
	public String findOriginalURLById(long id);
}
