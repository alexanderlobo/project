package com.surl.util;

public class ShortURLUtil {
	
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyz/_-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int BASE = CHARACTERS.length();

    public static long decodeURL(String encoded) {
        long uniqID = 0;
        for ( char c : encoded.toCharArray() )
        	uniqID = uniqID * BASE + CHARACTERS.indexOf(c);
        return uniqID;
    }   

    public static String encodeURL(long uniqID) {
        StringBuilder sb = new StringBuilder();
        while ( uniqID > 0 ) {
            sb.append(CHARACTERS.charAt((int)uniqID%BASE ) );
            uniqID /= BASE;
        }
        return sb.reverse().toString();   
    }

}