package com.surl.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.surl.model.ShortURLObj;

@Repository
public class ShortenRequestDao {

	@PersistenceContext
	private EntityManager entityManager;

	public void save(ShortURLObj urlObj) {
		entityManager.persist(urlObj);
	}

	public ShortURLObj getURLById(long id) {
		return entityManager.find(ShortURLObj.class, id);
	}

	public void delete(long id) {
		ShortURLObj urlObj = getURLById(id);
		if (urlObj != null) {
			entityManager.remove(urlObj);
		}
	}
}
