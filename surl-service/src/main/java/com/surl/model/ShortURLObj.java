package com.surl.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@Entity
@Table(name="urls")
@EntityScan("com.surl.model.ShortURLObj")
public class ShortURLObj {	

	@Id
    private Long id;
	
	private String originalURL;
	private String shortURL;	
	private LocalDateTime createdDate;
	private Long numHits;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOriginalURL() {
		return originalURL;
	}
	public void setOriginalURL(String originalURL) {
		this.originalURL = originalURL;
	}
	public String getShortURL() {
		return shortURL;
	}
	public void setShortURL(String shortURL) {
		this.shortURL = shortURL;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public Long getNumHits() {
		return numHits;
	}
	public void setNumHits(Long numHits) {
		this.numHits = numHits;
	}
	
	@Override
	public String toString() {
		return "Id:" + getId() + " Short: " + getShortURL() + " Long: " + getOriginalURL();
	}
}
